package com.prft.pmoservice.controller;

import static com.jayway.restassured.RestAssured.*;
//import static org.hamcrest.Matchers.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.jayway.restassured.RestAssured;
import com.prft.pmoservice.PMOServiceApplication;
import com.prft.pmoservice.entity.Project;

import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = PMOServiceApplication.class)
@TestPropertySource(value = { "classpath:application.properties" })
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
public class ProjectControllerTest {
	@Value("${server.port}")
	int port;

	@Before
	public void setBaseUri() {
		RestAssured.port = port;
		RestAssured.baseURI = "http://localhost"; // replace as appropriate
	}

	@Test
	public void getProjectsTest() {
		get("/api/project").then().assertThat().body(notNullValue());
		get("/api/project").then().assertThat().body("projectId[0]",equalTo(1));
	}
	
	
	@Test
	public void getStatusTest() {
		get("/api/project/status").then().assertThat().body("records.size",not(0));
	}
	
	@Test
	public void getActiveProjectTest() {
		get("/api/project/active-projects").then().assertThat().body("records.size",not(0));
	}
	
	@Test
	public void authenticateUserTest() {
		given().parameters(
				"username", "rakesh.kumbhare@perficient.com", 
				"password", "passw0rd")
		.when().post("/api/project/authenticateUser").then().body("isUserValid", equalTo("true"));
		
		given().parameters(
				"username", "rakesh.kumbhare@perficient.com", 
				"password", "wrongPassw0rd")
		.when().post("/api/project/authenticateUser").then().body("isUserValid", equalTo("false"));
	}

}
