/*
SQLyog Community v13.0.1 (64 bit)
MySQL - 5.6.41 : Database - docker_dev
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`docker_dev` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `docker_dev`;

/*Table structure for table `project_access` */

DROP TABLE IF EXISTS `project_access`;

CREATE TABLE `project_access` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `user_id` int(10) NOT NULL COMMENT 'User Id',
  `project_id` int(10) NOT NULL COMMENT 'Project ID',
  `access_level` char(5) DEFAULT NULL COMMENT 'Access Level',
  PRIMARY KEY (`id`),
  KEY `PROJECT_ACCESS_USER_ID` (`user_id`),
  KEY `PROJECT_ACCESS_PROJECT_ID` (`project_id`),
  CONSTRAINT `PROJECT_ACCESS_PROJECT_ID` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`) ON DELETE CASCADE,
  CONSTRAINT `PROJECT_ACCESS_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `project_access` */

/*Table structure for table `projects` */

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `project_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `project_name` varchar(255) DEFAULT NULL COMMENT 'Project Name',
  `project_desc` text COMMENT 'Project Description',
  `comments` text,
  `productivity` char(1) DEFAULT NULL COMMENT 'Productivity (R = Red, Y = Yellow, G = Green)',
  `quality` char(1) DEFAULT NULL COMMENT 'Quality (R = Red, Y = Yellow, G = Green)',
  `schedule` char(1) DEFAULT NULL COMMENT 'Schedule (R = Red, Y = Yellow, G = Green)',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Is Project Active (0 = Not Active, 1 = Active)',
  `start_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Project Start Date',
  `updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated Date',
  `end_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Project end date',
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `projects` */

insert  into `projects`(`project_id`,`project_name`,`project_desc`,`comments`,`productivity`,`quality`,`schedule`,`status`,`start_date`,`updated_date`,`end_date`) values 
(2,'Barco',NULL,NULL,'5','0','0',1,'2020-01-18 12:06:47','2020-01-18 12:12:16','2020-01-18 13:21:04');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `firstname` varchar(50) NOT NULL COMMENT 'Firstname',
  `lastname` varchar(50) DEFAULT NULL COMMENT 'Lastname',
  `username` varchar(255) NOT NULL COMMENT 'Username',
  `password` varchar(255) NOT NULL COMMENT 'Password',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'User status',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`user_id`,`firstname`,`lastname`,`username`,`password`,`status`) values 
(1,'Aniket','Nimje','aniket.nimje@perficient.com','e10adc3949ba59abbe56e057f20f883e',1),
(2,'Pranit','Borkar','pranit.borkar@perficient.com','e10adc3949ba59abbe56e057f20f883e',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
