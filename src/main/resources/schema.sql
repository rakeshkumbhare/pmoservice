CREATE TABLE project (
  project_id long NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  project_name varchar(255) NOT NULL COMMENT 'Project Name',
  project_desc text COMMENT 'Project Description',
  comments text,
  status varchar(255) NOT NULL DEFAULT 'GREEN' COMMENT 'Is Project Active (0 = RED, 1 = YELLOW, 2 = GREEN, active)',
  start_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Project Start Date',
  updated_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated Date',
  end_date datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'Project end date',
  PRIMARY KEY (project_id)
);


CREATE TABLE users (
  user_id long NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  firstname varchar(50) NOT NULL COMMENT 'Firstname',
  lastname varchar(50) DEFAULT NULL COMMENT 'Lastname',
  username varchar(255) NOT NULL COMMENT 'Username',
  password varchar(255) NOT NULL COMMENT 'Password',
  status integer NOT NULL DEFAULT '1' COMMENT 'User status',
  PRIMARY KEY (user_id)
) ;


CREATE TABLE project_access (
  id long NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  user_id long NOT NULL COMMENT 'User Id',
  project_id long NOT NULL COMMENT 'Project ID',
  access_level char(5) DEFAULT NULL COMMENT 'Access Level',
  PRIMARY KEY (id),
  CONSTRAINT PROJECT_ACCESS_PROJECT_ID FOREIGN KEY (project_id) REFERENCES project (project_id),
  CONSTRAINT PROJECT_ACCESS_USER_ID FOREIGN KEY (user_id) REFERENCES users (user_id) 
) ;