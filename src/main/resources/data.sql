/*Data for the table USERS */
insert  into users (user_id,firstname,lastname,username,password,status) values 
(1,'rakesh','kumbhare','rakesh@perficient.com','passw0rd',1);

/*Data for the table PROJECT */
insert into project (project_id, project_name, project_desc, comments, status, start_date, end_date, updated_date)
values (1, 'SSI', 'School Specialty Inco. US', 'no comment', 'GREEN', CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP);
insert into project (project_id, project_name, project_desc, comments,  status, start_date, end_date, updated_date)
values (2, 'CatarPillar', 'Finnix Catar Pillar US', 'no comment', 'YELLOW', CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP);
insert into project (project_id, project_name, project_desc, comments, status, start_date, end_date, updated_date)
values (3, 'KirkLand', 'Kirkland Furniture Mart US', 'no comment', 'GREEN', CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP);
insert into project (project_id, project_name, project_desc, comments, status, start_date, end_date, updated_date)
values (4, 'Ford', 'Ford car shopping', 'no comment', 'GREEN', CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP);
insert into project (project_id, project_name, project_desc, comments, status, start_date, end_date, updated_date)
values (5, 'EHI', 'EHI Holding', 'no comment', 'YELLOW', CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP);
insert into project (project_id, project_name, project_desc, comments, status, start_date, end_date, updated_date)
values (6, 'Fiserve', 'Fiserve Finance System', 'no comment', 'RED', CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP);
insert into project (project_id, project_name, project_desc, comments, status, start_date, end_date, updated_date)
values (7, 'Manchester', 'Manchester Health Care', 'no comment', 'RED', CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP);