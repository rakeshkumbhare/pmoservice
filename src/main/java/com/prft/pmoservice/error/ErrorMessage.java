package com.prft.pmoservice.error;

import java.sql.Timestamp;

import org.springframework.http.HttpStatus;

public class ErrorMessage {
	private HttpStatus status;
	private ErrorCode error;
	private String message;
	private String cause;
	private Timestamp timestamp;

	public ErrorMessage(HttpStatus status, ErrorCode error, String message, String cause, Timestamp timestamp) {
		this.status = status;
		this.error = error;
		this.message = message;
		this.cause = cause;
		this.timestamp = timestamp;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public ErrorCode getError() {
		return error;
	}

	public void setError(ErrorCode error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCause() {
		return cause;
	}

	public void setCause(String cause) {
		this.cause = cause;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

}
