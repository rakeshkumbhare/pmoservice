package com.prft.pmoservice.error;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.http.HttpStatus;

public class ErrorMessageHelper {

	public static ErrorMessage create(HttpStatus status, ErrorCode error, String message, String cause) {
		Timestamp timestamp = new Timestamp(new Date().getTime());
		ErrorMessage errorMessage = new ErrorMessage(status, error, message, cause, timestamp);
		return errorMessage;
	}

}
