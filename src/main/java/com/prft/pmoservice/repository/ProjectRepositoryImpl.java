package com.prft.pmoservice.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.prft.pmoservice.entity.Project;
import com.prft.pmoservice.vo.ProjectStatus;

@Repository
public class ProjectRepositoryImpl implements IProjectRepository {
	@Autowired
	private EntityManager entityManager;

	private Session getSession() {
		return entityManager.unwrap(Session.class);
	}

	@Override
	public List<ProjectStatus> getProjectStatus() {
		List<ProjectStatus> projectStatusList = new ArrayList<ProjectStatus>();
		try {
			Session session = getSession();
			Query query = session
					.createSQLQuery("SELECT status, count(status) AS status_count FROM PROJECT group by status");
			List list = query.list();
			if (list != null && !list.isEmpty()) {
				for (Object object : list) {
					Object[] result = (Object[]) object;
					ProjectStatus projectStatus = new ProjectStatus();
					projectStatus.setStatus(result[0].toString());
					projectStatus.setProject_count(Integer.parseInt(result[1].toString()));
					projectStatusList.add(projectStatus);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return projectStatusList;
	}

	@Override
	public boolean getAuthenticateUser(String username, String password) {
		boolean userFlag = false;
		try {
			Session session = getSession();
			Query query = session.createSQLQuery("SELECT * from users where username=? and password=?");
			query.setParameter(1, username);
			query.setParameter(2, password);
			List list = query.list();
			if (list != null && !list.isEmpty()) {
				userFlag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userFlag;
	}

	@Override
	public List<Project> getProjectByStatus(String status) throws Exception {
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(Project.class);
			criteria.add(Restrictions.eq("status", status.toUpperCase()));
			List<Project> projectList = criteria.list();
			return projectList;
		} catch (Exception e) {
			throw e;
		}
	}

}
