package com.prft.pmoservice.repository;

import java.util.List;

import com.prft.pmoservice.entity.Project;
import com.prft.pmoservice.vo.ProjectStatus;

public interface IProjectRepository {

	public List<ProjectStatus> getProjectStatus();
	public boolean getAuthenticateUser(final String username, final String password);
	public List<Project> getProjectByStatus(final String status) throws Exception;
}
