package com.prft.pmoservice.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.prft.pmoservice.entity.Project;
import com.prft.pmoservice.exception.ApplicationException;
import com.prft.pmoservice.exception.ServiceException;
import com.prft.pmoservice.service.ProjectService;
import com.prft.pmoservice.vo.ProjectStatus;

@RestController
@RequestMapping("/api/project")
public class ProjectController {
	@Autowired
	private ProjectService projectService;

	@GetMapping(value = "", produces = { "application/json" })
	public ResponseEntity<List<Project>> getProjects() {
		List<Project> projectList = new ArrayList<Project>();
		projectList = this.projectService.fetchAllProjects();
		return new ResponseEntity<List<Project>>(projectList, HttpStatus.OK);
	}

	@GetMapping(value = "/active-projects", produces = { "application/json" })
	public ResponseEntity<List<Project>> getActiveProjects() {
		List<Project> projectList = projectService.fetchActiveProjects();
		return new ResponseEntity<List<Project>>(projectList, HttpStatus.OK);
	}

	@PostMapping(value = "", consumes = { "application/json" }, produces = { "application/json" })
	public ResponseEntity<Map<String, Long>> createProject(@RequestBody Project project) {
		Long projectId = projectService.createProject(project);
		Map<String, Long> response = new HashMap<String, Long>();
		response.put("ProjectId", project.getProjectId());
		return new ResponseEntity<Map<String, Long>>(response, HttpStatus.OK);
	}

	@GetMapping(value = "/summary", produces = { "application/json" })
	public ResponseEntity<Map<String, List<ProjectStatus>>> getProjectStatus(HttpSession session) {
		if (session.getAttribute("LOGGEDIN") != null) {
			String loggedIn = session.getAttribute("LOGGEDIN").toString();
		}
		List<ProjectStatus> projectStatusList = projectService.getProjectStatus();
		Map<String, List<ProjectStatus>> response = new HashMap<String, List<ProjectStatus>>();
		response.put("records", projectStatusList);
		ResponseEntity<Map<String, List<ProjectStatus>>> responseEntity = new ResponseEntity<Map<String, List<ProjectStatus>>>(
				response, HttpStatus.OK);
		return responseEntity;
	}

	@GetMapping(value = "/status/{status}", produces = { "application/json" })
	public ResponseEntity<Map<String, List<Project>>> getProjectByStatus(@PathVariable("status") final String status)
			throws ApplicationException {
		try {
			List<Project> projectList = projectService.getProjectByStatus(status);
			Map<String, List<Project>> data = new HashMap<String, List<Project>>();
			data.put("projects", projectList);
			return new ResponseEntity<Map<String, List<Project>>>(data, HttpStatus.OK);
		} catch (ServiceException e) {
			throw new ApplicationException(e.getErrorCode(), e.getErrorMessage());
		}
	}

	@PostMapping(value = "/authenticateUser", produces = { "application/json" })
	public ResponseEntity<Map<String, String>> authenticateUser(@RequestParam String username,
			@RequestParam String password, HttpSession session) {
		boolean userFlag = projectService.getUserStatus(username, password);

		Map<String, String> usrResponse = new HashMap<String, String>();
		if (userFlag) {
			Map<String, String> userSessionData = new HashMap<String, String>();
			String JSESSIONID = session.getId();
			userSessionData.put("JSESSIONID", JSESSIONID);
			userSessionData.put("LOGGEDIN", username);

			String jsongToken = "";
			ObjectMapper mapper = new ObjectMapper();
			try {
				jsongToken = mapper.writeValueAsString(userSessionData);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}

			Encoder encoder = Base64.getEncoder();
			String accessToken = encoder.encodeToString(jsongToken.getBytes());
			usrResponse.put("access_token", accessToken);
		}

		usrResponse.put("isUserValid", "" + userFlag);

		ResponseEntity<Map<String, String>> responseEntity = new ResponseEntity<Map<String, String>>(usrResponse,
				HttpStatus.OK);

		return responseEntity;
	}

}
