package com.prft.pmoservice.vo;

public class ProjectStatus {

	private String status;
	private Integer project_count;

	public ProjectStatus() {
	}

	public String getStatus() {
		return status;
	}

	public ProjectStatus(String status, Integer project_count) {
		super();
		this.status = status;
		this.project_count = project_count;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getProject_count() {
		return project_count;
	}

	public void setProject_count(Integer project_count) {
		this.project_count = project_count;
	}

}
