package com.prft.pmoservice.exception;

import java.util.Arrays;

import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.prft.pmoservice.error.ErrorCode;
import com.prft.pmoservice.error.ErrorMessage;
import com.prft.pmoservice.error.ErrorMessageHelper;

@ControllerAdvice
public class ApplicationExceptionController {

	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<ErrorMessage> hanldeNotFoundException(NotFoundException e) {
		ErrorMessage errorMessage = ErrorMessageHelper.create(HttpStatus.NOT_FOUND, ErrorCode._NOT_FOUND_ERROR, "Requeste URL not found", null);
		ResponseEntity<ErrorMessage>  response = new ResponseEntity<ErrorMessage>(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
		return response;
	}

	@ExceptionHandler(ApplicationException.class)
	public ResponseEntity<ErrorMessage> handleApplicationException(ApplicationException e) {
		ErrorMessage errorMessage;
		ResponseEntity<ErrorMessage> response;

		switch (e.getErrorCode()) {
		case _DATA_ACCESS_ERROR:
			errorMessage = ErrorMessageHelper.create(HttpStatus.INTERNAL_SERVER_ERROR, e.getErrorCode(),
					e.getErrorMessage(), Arrays.toString(e.getStackTrace()));
			response = new ResponseEntity<ErrorMessage>(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
			return response;
		default:
			errorMessage = ErrorMessageHelper.create(HttpStatus.INTERNAL_SERVER_ERROR, e.getErrorCode(),
					e.getErrorMessage(), Arrays.toString(e.getStackTrace()));
			response = new ResponseEntity<ErrorMessage>(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
			return response;
		}
	}
}
