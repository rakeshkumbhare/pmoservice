package com.prft.pmoservice.exception;

import com.prft.pmoservice.error.ErrorCode;

public class ApplicationException extends Exception {
	private static final long serialVersionUID = 6822598461736789470L;
	private ErrorCode errorCode;
	private String errorMessage;

	public ApplicationException(ErrorCode errorCode, String errorMessage) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public ErrorCode getErrorCode() {
		return errorCode;
	}
}
