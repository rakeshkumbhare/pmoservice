package com.prft.pmoservice.exception;

import com.prft.pmoservice.error.ErrorCode;

public class ServiceException extends Exception {
	private static final long serialVersionUID = -5784286223926255653L;
	private ErrorCode errorCode;
	private String errorMessage;

	public ServiceException(ErrorCode errorCode, String errorMessage) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public ErrorCode getErrorCode() {
		return errorCode;
	}

}
