package com.prft.pmoservice.interceptor;

import java.util.Base64;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class PMOServiceInterceptor implements HandlerInterceptor {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		if (request.getHeader("access_token") != null) {
					String accessToken = request.getHeader("access_token");
					String jsonToken = new String(Base64.getDecoder().decode(accessToken));
					Map sessionData = new ObjectMapper().readValue(jsonToken, Map.class);
					String loggedIn = (String) sessionData.get("LOGGEDIN");
					request.getSession().setAttribute("LOGGEDIN", loggedIn);
		}
		return HandlerInterceptor.super.preHandle(request, response, handler);
	}

}
