package com.prft.pmoservice.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.prft.pmoservice.entity.Project;
import com.prft.pmoservice.error.ErrorCode;
import com.prft.pmoservice.exception.ServiceException;
import com.prft.pmoservice.repository.ProjectRepository;
import com.prft.pmoservice.vo.ProjectStatus;

@Service
public class ProjectService {
	@Autowired
	private ProjectRepository projectRepository;

	public List<Project> fetchAllProjects() {
		return projectRepository.findAll();
	}

	public List<Project> fetchActiveProjects() {
		List<Project> projectList = fetchAllProjects();
		if (projectList != null & !projectList.isEmpty()) {
//			List<Project> activeProjectList = projectList.stream().filter(project -> project.getStatus().equals("1"))
//					.collect(Collectors.toList());
			// List<Project> ProjectList = projectList;
			return projectList;
		}
		return null;
//		} else {
//			return projectList;
//		}
	}

	public Long createProject(Project project) {
		project.setStartDate(new Timestamp(new Date().getTime()));
		project.setUpdateDate(new Timestamp(new Date().getTime()));
		if (StringUtils.isEmpty(project.getProjectDesc())) {
			project.setProjectDesc(project.getProjectName());
		}
		projectRepository.saveAndFlush(project);
		return project.getProjectId();
	}

	public List<ProjectStatus> getProjectStatus() {
		List<ProjectStatus> projectStatusList = new ArrayList<ProjectStatus>();
		try {
			projectStatusList = projectRepository.getProjectStatus();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return projectStatusList;
	}

	public List<Project> getProjectByStatus(String status) throws com.prft.pmoservice.exception.ServiceException {
		List<Project> projectList = new ArrayList<Project>();
		try {
			projectList = projectRepository.getProjectByStatus(status);
			return projectList;
		} catch (DataAccessException e) {
			throw new ServiceException(ErrorCode._DATA_ACCESS_ERROR,
					"Error while fetching Projects by status : " + status);
		} catch (Exception e) {
			throw new ServiceException(ErrorCode._GENERIC_ERROR, "Error while fetching Projects by status : " + status);
		}
	}

	public boolean getUserStatus(String username, String password) {
		boolean userFlag = false;
		try {
			userFlag = projectRepository.getAuthenticateUser(username, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userFlag;
	}

}
