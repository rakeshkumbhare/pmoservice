package com.prft.pmoservice.controller;

import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.prft.pmoservice.service.ProjectService;
import com.prft.pmoservice.vo.ProjectStatus;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProjectControllerTest_rakesh {

	private static final ObjectMapper om = new ObjectMapper();

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private ProjectService projectService;
	
	@Before
    public void init() {
		List<ProjectStatus> projectStatusList = new ArrayList<ProjectStatus>();
//		projectStatusList.add(new ProjectStatus(0, 3));
//		projectStatusList.add(new ProjectStatus(1, 2));
//		projectStatusList.add(new ProjectStatus(2, 1));
		
        when(projectService.getProjectStatus()).thenReturn(projectStatusList);
    }
	
	
	@Test
    public void getProjectStatus_OK() throws Exception {
        mockMvc.perform(get("/api/project/status"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        
        verify(projectService, timeout(1)).getProjectStatus();
    }

}
